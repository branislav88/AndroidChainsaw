/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import static shared.Methods.driver;
import static shared.Methods.log;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author branislavgrbic
 */
public class StartingApplication extends shared.Methods {

    public static void StartingApp() throws InterruptedException {

        try {
            log("11111111");
            WebDriverWait appstarting = new WebDriverWait(driver, 1000);
            log("222222");
            appstarting.until(ExpectedConditions.presenceOfElementLocated(By.id("co.codemind.meridianbetbeta.debug:id/root_layout")));
            log("3333333");
//        log("Aplikacija je usešno učitana");
            Thread.sleep(1000);
        } catch (NullPointerException exception) {
            log(exception.getCause());
        }
    }

}
