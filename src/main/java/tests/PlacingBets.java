/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import shared.Commands;

/**
 *
 * @author branislavgrbic
 */
public class PlacingBets extends Commands {
    
    public static void SingleBet() throws InterruptedException {

        /* KROZ MENU/SPORTS/LEAGUE STRUKTURU ODABIRAM PRVU RANDOM SELEKCIJU KOJU NAĐEM I DODAJEM JE NA TIKET */
        WebElement HB = byID(HomeButton);
        HB.click();
        
        WebElement FootballOption = byxpath(FootballFromList);
        FootballOption.click();
        
        Thread.sleep(3000);
        WebElement eventList = byID("co.codemind.meridianbetbeta.debug:id/events_list");
        List<WebElement> oddslist1 = eventList.findElements(By.id("co.codemind.meridianbetbeta.debug:id/text_price_1")) ;
        Thread.sleep(200);
        
        Iterator<WebElement> i = oddslist1.iterator();
        Thread.sleep(250);


        /*  ZAPISI SVE LIGE I UKUPAN BROJ   */
        log("Ukupno vidljivih selekcija na poziciji 1 je: " + oddslist1.size());
        
        while (i.hasNext()) {
            WebElement row = i.next();
            log("Selekcija 1 = "+row.getText());
        }
        Thread.sleep(250);
        
    }
    
    public static void MultiBet() {
        
    }
    
    public static void SistemBet() {
        
    }
    
    public static void SistemWithFixBet() {
        
    }
    
}
