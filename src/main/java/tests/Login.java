/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import org.openqa.selenium.WebElement;
import resources.ReadFromPropertiesFile;
import static resources.ReadFromPropertiesFile.prop;

/**
 *
 * @author branislavgrbic
 */
public class Login extends shared.Methods {

    public static void Login() throws InterruptedException {
        WebElement ImageProfile = byID("co.codemind.meridianbetbeta.debug:id/image_profile");
        ImageProfile.click();

        Thread.sleep(400);
        WebElement LoginButton = byID("co.codemind.meridianbetbeta.debug:id/button_login_logout_user");
        LoginButton.click();

        Thread.sleep(400);

        WebElement username = byID("co.codemind.meridianbetbeta.debug:id/input_email");
        username.sendKeys(prop.getProperty(ReadFromPropertiesFile.username));

        Thread.sleep(400);

        WebElement password = byID("co.codemind.meridianbetbeta.debug:id/input_password");
        password.sendKeys(prop.getProperty(ReadFromPropertiesFile.password));

        Thread.sleep(400);

        WebElement LoginAction = byID("co.codemind.meridianbetbeta.debug:id/login_button");
        LoginAction.click();

        Thread.sleep(5000);
        try {

            if (byID("co.codemind.meridianbetbeta.debug:id/txt_hello").isDisplayed()) {
                log("Logovanje uspešno, test se nastavlja");
            } else if (byID("co.codemind.meridianbetbeta.debug:id/md_titleFrame").isDisplayed()) {
                log("Logovanje neuspešno, test se prekida");
                throw new Error("Greška prilikom logovanja");
            }
        } catch (Exception e) {
        }

        WebElement CloseButton = byID("co.codemind.meridianbetbeta.debug:id/btn_close");
        CloseButton.click();
    }

}
