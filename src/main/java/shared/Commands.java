/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import org.openqa.selenium.WebElement;

/**
 *
 * @author branislavgrbic
 */
public class Commands extends shared.Methods {
    
    public static String HomeButton = "co.codemind.meridianbetbeta.debug:id/left_menu_button";
    public static String BetSlipButton = "co.codemind.meridianbetbeta.debug:id/bet_slip_button";
    public static String HorizontalMenu = "co.codemind.meridianbetbeta.debug:id/recycler_view_menu_items";
    public static String SportsList = "co.codemind.meridianbetbeta.debug:id/rv_sports";
    public static String FootballFromList = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView[2]/android.widget.RelativeLayout[1]";
    
}
