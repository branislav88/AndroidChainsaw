/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 *
 * @author branislavgrbic
 */
public class Methods {

    /*DODAT ANDROID DRIVER*/
    public static AndroidDriver driver;
    /*DODAT ANDROID DRIVER*/

    private WebElement el;

    public static void log(Object tekst) {
        System.out.println(tekst);
    }

    public static void sleep(Object tekst) throws InterruptedException {
        Thread.sleep(0);
    }

    /*
    WEBDRIVER METHODS
     */
    public static WebElement byID(String id) {
        try {
            return driver.findElement(By.id(id));
        } catch (NoSuchElementException e) {

            return null;
        }
    }

    public static WebElement byCSS(String selector) {
        try {
            return driver.findElement(By.cssSelector(selector));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public static WebElement byxpath(String xpathExpression) {
        try {
            return driver.findElement(By.xpath(xpathExpression));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public static WebElement bylinkText(String linkText) {
        try {
            return driver.findElement(By.linkText(linkText));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public static WebElement byClassName(String className) {
        try {
            return driver.findElement(By.className(className));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public void findElementByIdAndSendKeys(String id, String keys) {
        el = byID(id);
        sendKeys(el, keys);
    }

    public void findElementByXpathAndSendKeys(String xPath, String keys) {
        el = byxpath(xPath);
        sendKeys(el, keys);
    }

    public void findElementByCSSAndSendKeys(String css, String keys) {
        el = byCSS(css);
        sendKeys(el, keys);
    }

    public void findElementByLinkTextAndSendKeys(String linktext, String keys) {
        el = bylinkText(linktext);
        sendKeys(el, keys);
    }

    /* NOT SUPPORTET ON APPIUM */
    
//    public void FindByIDandClick(String id) {
//        el = byID(id);
//        click(el);
//    }
//
//    public void byXpathandClick(String xpathExpression) {
//        el = byID(xpathExpression);
//        click(el);
//    }
//
//    public void byCssandClick(String selector) {
//        el = byID(selector);
//        click(el);
//    }
//
//    public void bylinkTextandClick(String linkText) {
//        el = byID(linkText);
//        click(el);
//    }
//
//    public static void click(WebElement el) {
//        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", el);
//    }

    public static void sendKeys(WebElement el, String keys) {
        el.sendKeys(keys);
//        sendKeys(el, keys);
    }
}
