package resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static shared.Methods.log;

public class ReadFromPropertiesFile {

    public static Properties prop;
    public static Properties tests;

    public static String app_path = "APP_PATH";
    public static String appium_address = "APPIUM_ADDRESS";
    public static String device_name = "DEVICE_NAME";
    public static String platform_name = "PLATFORM_NAME";
    public static String platform_version = "PLATFORM_VERSION";
    public static String user = "USER";
    public static String username = "USERNAME";
    public static String password = "PASSWORD";
    public static String login = "LOGIN";
    public static String favorites = "FAVORITES";
    public static String language = "LANGUAGE";
    public static String oddsformat = "ODDSFORMAT";
    public static String depositwithdraw = "DEPOSITWITHDRAW";
    public static String directbet = "DIRECTBET";
    public static String fastbet = "FASTBET";
    public static String footernav = "FOOTERNAV";
    public static String lastticket = "LASTTICKET";
    public static String logout = "LOGOUT";
    public static String myaccount = "MYACCOUNT";
    public static String placebet = "PLACEBET";
    public static String recentticket = "RECENTTICKET";
    public static String refreshbalance = "REFRESHBALANCE";
    public static String registration = "REGISTRATION";
    public static String scroll = "SCROLL";
    public static String search = "SEARCH";
    public static String sidebarnav = "SIDEBARNAV";
    public static String smartticket = "SMARTTICKET";
    public static String sporteventstructure = "SPORTEVENTSTRUCTURE";
    public static String ticketdetails = "TICKETDETAILS";


    /*PROCITAJ CONF*/
    public static void ReadProperties() {

        prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("src/main/java/resources/config.properties");

            /* UCITAJ FAJL*/
            prop.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*PROCITAJ CONF*/
    public static void ReadTestProperties() {

        tests = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("src/main/java/resources/test.properties");

            /* UCITAJ FAJL*/
            tests.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*STAMPAJ CONF*/
    public static void PrintProperties() {

        log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log("Lista svih properties parametara");
        log("APP_PATH: " + prop.getProperty(app_path));
        log("APPIUM_ADDRESS: " + prop.getProperty(appium_address));
        log("DEVICE_NAME: " + prop.getProperty(device_name));
        log("PLATFORM_NAME: " + prop.getProperty(platform_name));
        log("PLATFORM_VERSION: " + prop.getProperty(platform_version));
        log("USER: " + prop.getProperty(user));
        log("USERNAME: " + prop.getProperty(username));
        log("PASSWORD: " + prop.getProperty(password));        
        log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    /*STAMPAJ CONF*/
    public static void PrintTestProperties() {

        log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log("Lista svih test properties parametara");
        log("Test " + login +" = " + tests.getProperty(login));
        log("Test " + favorites +" = " + tests.getProperty(favorites));
        log("Test " + language +" = " + tests.getProperty(language));
        log("Test " + oddsformat +" = " + tests.getProperty(oddsformat));
        log("Test " + depositwithdraw +" = " + tests.getProperty(depositwithdraw));
        log("Test " + directbet +" = " + tests.getProperty(directbet));
        log("Test " + fastbet +" = " + tests.getProperty(fastbet));
        log("Test " + footernav +" = " + tests.getProperty(footernav));
        log("Test " + lastticket +" = " + tests.getProperty(lastticket));
        log("Test " + logout +" = " + tests.getProperty(logout));
        log("Test " + myaccount +" = " + tests.getProperty(myaccount));
        log("Test " + placebet +" = " + tests.getProperty(placebet));
        log("Test " + recentticket +" = " + tests.getProperty(recentticket));
        log("Test " + refreshbalance +" = " + tests.getProperty(refreshbalance));
        log("Test " + registration +" = " + tests.getProperty(registration));
        log("Test " + scroll +" = " + tests.getProperty(scroll));
        log("Test " + search +" = " + tests.getProperty(search));
        log("Test " + sidebarnav +" = " + tests.getProperty(sidebarnav));
        log("Test " + smartticket +" = " + tests.getProperty(smartticket));
        log("Test " + sporteventstructure +" = " + tests.getProperty(sporteventstructure));
        log("Test " + ticketdetails +" = " + tests.getProperty(ticketdetails));
        log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
    
}
