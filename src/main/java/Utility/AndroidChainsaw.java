/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import shared.Methods;
import static Utility.StartAppium.StartAppium;
import static Utility.TestRunner.TestRunner;
import io.appium.java_client.android.AndroidDriver;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import static resources.ReadFromPropertiesFile.*;

/**
 *
 * @author branislavgrbic
 */
public class AndroidChainsaw extends Methods {

    /**
     * @param args the command line
     * arguments< app_path, appium_address, device_name, platform_name, platform_version, user>
     * @throws java.net.MalformedURLException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws MalformedURLException, InterruptedException, IOException {
       
        StartAppium();

        /* READ AND PRINT APPIUM CONFIGURATION */
        ReadProperties();
        PrintProperties();
        /* READ AND PRINT APPIUM CONFIGURATION */

        /* READ AND PRINT TESTINF CONFIGURATION */
        ReadTestProperties();
        PrintTestProperties();
        /* READ AND PRINT TESTINF CONFIGURATION */

        /* CONNECT WITH THE DEVICE */
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", prop.getProperty(device_name));
        capabilities.setCapability("platformName", prop.getProperty(platform_name));
        capabilities.setCapability("platformVersion", prop.getProperty(platform_version));
        capabilities.setCapability("app", prop.getProperty(app_path));
        driver = new AndroidDriver(new URL(prop.getProperty(appium_address)), capabilities);
        /* CONNECT WITH THE DEVICE */
        Thread.sleep(30000);
        /* TESTING */
        TestRunner();
        /* TESTING */

    }

}
