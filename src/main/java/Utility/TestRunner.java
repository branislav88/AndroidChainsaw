/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import static shared.Methods.driver;
import static shared.Methods.log;
import resources.ReadFromPropertiesFile;
import static resources.ReadFromPropertiesFile.*;
import static tests.Login.Login;
import static tests.PlacingBets.SingleBet;
import static tests.StartingApplication.StartingApp;

/**
 *
 * @author branislavgrbic
 */
public class TestRunner extends ReadFromPropertiesFile {

    public static void TestRunner() throws InterruptedException {

        /*  WAIT APP TO START  */
//        Thread.sleep(1000);
//        log("krecem");
//        StartingApp();
//        Thread.sleep(1000);
//        log("zavrsio");
        /*  WAIT APP TO START  */
        if (tests.getProperty(login).contains("true")) {
            log("Početak Login testa");
            Login();
            log("Kraj Login testa");
        } else {

        }

        if (tests.getProperty(placebet).contains("true")) {
            SingleBet();
        } else {

        }

        if (tests.getProperty(myaccount).contains("true")) {

        } else {

        }

        if (tests.getProperty(depositwithdraw).contains("true")) {

        } else {

        }

        if (tests.getProperty(directbet).contains("true")) {

        } else {

        }

        if (tests.getProperty(fastbet).contains("true")) {

        } else {

        }

        if (tests.getProperty(favorites).contains("true")) {

        } else {

        }

        if (tests.getProperty(footernav).contains("true")) {

        } else {

        }

        if (tests.getProperty(language).contains("true")) {

        } else {

        }

        if (tests.getProperty(lastticket).contains("true")) {

        } else {

        }

        if (tests.getProperty(logout).contains("true")) {

        } else {

        }

        if (tests.getProperty(oddsformat).contains("true")) {

        } else {

        }

        if (tests.getProperty(recentticket).contains("true")) {

        } else {

        }

        if (tests.getProperty(refreshbalance).contains("true")) {

        } else {

        }

        if (tests.getProperty(registration).contains("true")) {

        } else {

        }

        if (tests.getProperty(scroll).contains("true")) {

        } else {

        }

        if (tests.getProperty(search).contains("true")) {

        } else {

        }
        if (tests.getProperty(sidebarnav).contains("true")) {

        } else {

        }
        if (tests.getProperty(smartticket).contains("true")) {

        } else {

        }
        if (tests.getProperty(sporteventstructure).contains("true")) {

        } else {

        }
        if (tests.getProperty(ticketdetails).contains("true")) {

        } else {

        }
        /*TEST IS DONE*/
        log("Test was done succesfull");
        driver.close();
        /*TEST IS DONE*/
    }

}
