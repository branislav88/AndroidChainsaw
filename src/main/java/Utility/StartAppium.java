/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import static shared.Methods.log;
import java.io.IOException;

/**
 *
 * @author branislavgrbic
 */
public class StartAppium {

    public static void StartAppium() throws InterruptedException, IOException {
        log("Startujem appium");
        Runtime.getRuntime().exec("/usr/bin/open -a Terminal /usr/local/bin/appium");
        Thread.sleep(8000);
    }

}
